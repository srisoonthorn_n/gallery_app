export const setToken = (taketoken) => ({
    type: "SET_TOKEN",
    token: taketoken
})

export const setUser = (takeUser) => ({
    type: "SET_USER",
    user: takeUser
})