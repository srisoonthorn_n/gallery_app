import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Data from '../Data';
import { Modal, Button, Fade, Backdrop } from '@material-ui/core';
import axios from 'axios';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import AddImage from './AddImage';
import { useSelector } from 'react-redux';
import { listImages, host, listImagesPublish, } from '../api';
import { useParams } from 'react-router-dom';
import { Col, Row } from 'antd';

function rand() {
    return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const AllPhoto = () => {

    const [images, setImages] = useState([]);
    const [open, setOpen] = React.useState(false);
    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);
    const { id } = useParams();

    const token = useSelector(state => state.token);



    useEffect(() => {
        listImagesPublish(id)
            .then((response) => {
                console.log(response);

                setImages(response.data);
            })
            .catch((err) => console.log(err));
    }, [])


    return (
        <div className="containerPhotoList">
            <div className="bodyPhotoList">
                <div className="postPhotoList">

                    {
                        images == "" || images == null ?
                            <div className="postNoPhotoList">
                                <div className="postPhotoNoData">
                                    <div style={{ width: '100%' }}>
                                        <h2 style={{ color: "#FFFFFF", position: 'center', marginRight: '53px' }}>No Image</h2>
                                    </div>
                                </div>
                            </div>
                            :



                            images.map((tile, index) => {

                                return (

                                    <div>

                                        <GridList cellHeight={500} className={classes.gridList} key={index}>
                                            <GridListTile className="photoContainerList" key={tile.img}>
                                                <img src={`${host}/upload/${id}/${tile.Filename}`} />
                                            </GridListTile>
                                        </GridList>

                                    </div>

                                )


                            })
                    }



                </div>
            </div>



        </div >
    )
}

export default AllPhoto


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 'auto',
        height: 'auto',
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: '24px'


    },
    modalBackDrop: {
        backgroundColor: ' rgba(0, 0, 0, 0.13) !important'
    },
    fab: {
        color: '#FFFFFF',
        position: 'absolute',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
        backgroundColor: '#FFFFFF',
        marginRight: '17px',
    },

}));