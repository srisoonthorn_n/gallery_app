import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import { GridListTileBar } from '@material-ui/core';
import { listGallery, listAllGallery } from '../api';
import Data from '../Data';
import { useSelector, useDispatch } from 'react-redux';




const AllGallery = () => {
    const [gallerys, setGallerys] = useState([]);
    const [nameGallery, setNameGallery] = useState("");
    const classes = useStyles();
    const token = useSelector(state => state.token);

    console.log("Token All Gallery", token);



    const dispatch = useDispatch();


    useEffect(() => {
        listAllGallery(token)
            .then((response) => {
                console.log(response)
                setGallerys(response.data)

            })
            .catch((err) => console.error(err))
    }, [])


    return (
        <div className="containerGallery">
            <div className="bodyGallery">
                <div className="postPhotoGallery">

                    {
                        gallerys == "" ?
                            <div className="postGalleryNoData">
                                <div style={{ marginTop: 200 }}>
                                    <h2 style={{ color: "#FFFFFF" }}>No Gallery</h2>
                                </div>
                            </div>
                            :
                            gallerys.map((item, index) => (

                                <GridList cellHeight={500} className={classes.gridList} className="GridList" key={index}>

                                    <GridListTile className="photoContainerGallery" key={item.img} >
                                        <a className="link_menu" href={"/allphoto/" + item.id} >
                                            {/* <img src={require('../unnamed.png')} alt={item.name} className="imageGallery" /> */}
                                            <img src="https://assets.materialup.com/uploads/ae582946-db31-4d15-989b-ca420dea8295/gallery_big.png" alt={item.name} className="imageGallery" />
                                        </a>
                                        <GridListTileBar
                                            title={item.name}
                                            classes={{
                                                root: classes.titleBar,
                                                title: classes.title,
                                            }}
                                            actionIcon={
                                                <div>

                                                </div>
                                            }
                                        />
                                    </GridListTile>
                                </GridList >

                            ))
                    }
                </div >
            </div >
        </div >
    )
}

export default AllGallery


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 'auto',
        height: 'auto',
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '20%',
        height: '30%'

    },
}));