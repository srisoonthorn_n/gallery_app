import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Modal, Button, Fade, Backdrop, Typography, IconButton, GridListTileBar, Card } from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { useSelector } from 'react-redux';
import { listImages, getOne, addImage, host, deleteImage } from '../api';
import { useParams } from 'react-router-dom';
import { Upload, Col, Row } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

import Meta from 'antd/lib/card/Meta';

import Delete from '@material-ui/icons/Delete';

function rand() {
    return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const PhotoList = () => {

    const [images, setImages] = useState([]);
    const [open, setOpen] = React.useState(false);
    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);
    const { id } = useParams();



    const token = useSelector(state => state.token);
    console.log("tokennnnnnnnnnnn", token);

    const [idGallery, setIdGallery] = useState([])


    useEffect(() => {
        listImages(id, token)
            .then((response) => {
                console.log(response.data);

                setImages(response.data);
            })
            .catch((err) => console.log(err));

        getOne(id)
            .then((response) => {
                setIdGallery(response.data)
            })
            .catch((err) => console.log(err));
    }, images)




    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        getOne(id)
            .then((response) => {
                const id = response.data.id
                const files = e.target.elements["photos"].files;
                const form = new FormData();
                for (let i = 0; i < files.length; i++) {
                    form.append("photos", files[i]);
                }
                addImage({ galleryID: id, form }, token)
                    .then((response) => {
                        console.log("submit", response);

                        setImages([...images, response.data]);

                    })
                    .catch((err) => {
                        console.log(err);
                    });
                handleClose()
            })
            .catch((err) => { console.log(err) })

    };



    const handleDelete = (value) => {
        const filtered = images.filter((item) => item.id !== value.id);
        setImages(filtered);
        console.log("delete", value.id)

        deleteImage(value.id, token)
            .then(res => {
                console.log(res)
            })
            .catch(err => {
                console.log(err)
            })

        alert("Delete")
    }





    const [queueImages, setQueuemages] = useState([]);

    const fileChange = ({ fileList }) => {
        console.log("Called image update")

        setQueuemages(fileList)

    }

    const uploadQueuedImages = () => {
        if (queueImages.length > 0) {
            const form = new FormData()
            for (var i = 0; i < queueImages.length; i++) {
                form.append("photos", queueImages[i].originFileObj)
            }
            addImage(id, form, token).then(res => {
                console.log("RESULT", res.data)
                setImages([...res.data, ...images])
                handleClose()
            }).catch(err => console.error("ERRRR", err))
            return
        }
        alert("error no file")
    }

    return (
        <div className="containerPhotoList">
            <div className="bodyPhotoList">
                <div className="postMyPhotoList">
                    <Row>
                        {
                            images == "" ?
                                <div className="postNoPhotoList">
                                    <div className="postPhotoNoData">
                                        <div style={{ width: '100%' }}>
                                            <h2 style={{ color: "#FFFFFF", position: 'center', marginRight: '53px' }}>No Image</h2>
                                        </div>
                                    </div>
                                </div>
                                :
                                images.map((tile, index) => (
                                    <Row span={4} style={{ margin: 10 }}>
                                        <Card style={{ height: 389 }}>
                                            <img
                                                style={{ height: 310, width: 431, margin: 10 }}
                                                src={`${host}/${tile.filename}`}
                                            />
                                            <Meta description={
                                                <Button aria-controls="simple-menu" aria-haspopup="true" variant="contained" className="buttonSetting" onClick={() => handleDelete(tile)}>
                                                    <Delete style={{ color: "rgb(255, 79, 79)" }} />
                                                </Button>
                                            }>

                                            </Meta>
                                        </Card>





                                    </Row>



                                ))
                        }
                    </Row>


                </div>
            </div>


            <Fab className={classes.fab}>
                <Typography variant="h6" className="title1" >
                    <Fab color="#FFFFFF" size="medium" aria-label="add" onClick={handleOpen} style={{ backgroundColor: "#000000" }} >
                        <AddIcon style={{ color: '#FFFFFF' }} />
                    </Fab>
                </Typography>
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={open}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={open}>
                        <div className={classes.paper}>
                            <h2 id="transition-modal-title">Add Photo</h2>
                            <div style={{ textAlign: 'center', marginTop: '50px' }}>

                                <Upload multiple
                                    onChange={(e) => fileChange(e)}

                                >
                                    <Button>
                                        <UploadOutlined /> Upload
                                    </Button>
                                </Upload>


                                <div style={{ marginTop: 30 }}>
                                    <button className="buttonUpload" type="submit" variant="contained" color="primary" component="span" onClick={uploadQueuedImages}>
                                        Upload
                                    </button>
                                </div>



                            </div>


                        </div>
                    </Fade>
                </Modal>

            </Fab>

        </div >
    )
}

export default PhotoList


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 'auto',
        height: 'auto',
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: '24px'


    },
    modalBackDrop: {
        backgroundColor: ' rgba(0, 0, 0, 0.13) !important'
    },
    fab: {
        color: '#FFFFFF',
        position: 'absolute',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
        backgroundColor: '#FFFFFF',
        marginRight: '17px',
    },

}));