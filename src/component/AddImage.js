import React, { useState, useEffect } from 'react';
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Fab, Fade, TextField, Button, formatMs } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Backdrop from '@material-ui/core/Backdrop';
import axios from 'axios';
import { host, addImage, getOne } from '../api/index'
import { Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';



function rand() {
    return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}



const AddImage = () => {

    const [message, setMessage] = useState("");
    const [images, setImages] = useState([]);
    const { id } = useParams()
    const token = useSelector(state => state.token);
    const [idGallery, setIdGallery] = useState(0)

    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);
    const [open, setOpen] = React.useState(false);


    // useEffect(() => {
    //     getOne(id)
    //         .then((response) => {
    //             console.log(response.data.id);

    //             setIdGallery(response.data.id)
    //         })
    //         .catch((err) => { console.log(err) })
    // })


    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        getOne(id)
            .then((response) => {
                const id = response.data.id
                const files = e.target.elements["photos"].files;
                const form = new FormData();
                for (let i = 0; i < files.length; i++) {
                    form.append("photos", files[i]);
                }
                addImage({ galleryID: id, form }, token)
                    .then((response) => {
                        console.log(response);

                        setImages([...images, response.data]);

                    })
                    .catch((err) => {
                        console.log(err);
                    });
                handleClose()
            })
            .catch((err) => { console.log(err) })

    };


    return (
        <div>
            <Typography variant="h6" className="title1" >
                <Fab color="#FFFFFF" size="medium" aria-label="add" onClick={handleOpen} style={{ backgroundColor: "#000000" }} >
                    <AddIcon style={{ color: '#FFFFFF' }} />
                </Fab>
            </Typography>

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h2 id="transition-modal-title">Add Photo</h2>
                        <div style={{ textAlign: 'center', marginTop: '50px' }}>


                            {/* <label htmlFor="contained-button-file"> */}
                            <form
                                method="POST"
                                action="/upload"
                                encType="multipart/form-data"
                                onSubmit={() => handleSubmit(id)}
                            >
                                {/* <input accept="image/*" type="file" name="photos" multiple /> */}

                                <Upload multiple
                                    onChange={({ file, fileList, event }) => {

                                        setImages(fileList.map((f) => f.originFileObj));
                                    }}

                                >
                                    <Button>
                                        <UploadOutlined /> Upload
                                    </Button>
                                </Upload>


                                <div style={{ marginTop: 30 }}>
                                    <button className="buttonUpload" type="submit" variant="contained" color="primary" component="span">
                                        Upload
                                    </button>
                                </div>

                            </form>

                            {/* <form
                                    method="POST"
                                    action="/upload"
                                    encType="multipart/form-data"
                                    onSubmit={handleSubmit}
                                >
                                    <input type="file" name="photos" multiple />
                                    <button type="submit">upload</button>
                                </form> */}

                            {/* </label> */}

                        </div>


                    </div>
                </Fade>
            </Modal>

        </div >
    )
}

export default AddImage

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '20%',
        height: '30%'

    },
}));
