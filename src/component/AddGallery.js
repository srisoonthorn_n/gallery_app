import React, { useState } from 'react';
import { Layout, Breadcrumb, Card, Input, Button, Row, Col, Upload } from 'antd';

import axios from 'axios';
import { useSelector } from 'react-redux';
import { UploadOutlined } from '@ant-design/icons';
import { host } from '../api/index';
import { useHistory } from 'react-router-dom';
import { Container, Typography, TextField } from '@material-ui/core';

const { Content } = Layout;


const AddGallery = () => {


    const [galleryName, setGalleryName] = useState("")
    const [images, setImages] = useState([])
    const status_publish = false;
    const history = useHistory()

    const token = useSelector(state => state.token)

    const addGallery = async (e) => {

        await axios.post(`${host}/auth/galleries`, { name: galleryName, status_publish: status_publish }, {
            headers:
            {
                Authorization: `Bearer ${token}`,
            }
        })
            .then(response => {

                const id = response.data.id

                // const files = e.target.elements["photos"].files;
                const form = new FormData();
                for (let i = 0; i < images.length; i++) {
                    form.append("photos", images[i]);
                }
                axios.post(`${host}/auth/galleries/` + id + `/images`, form, {
                    headers:
                    {
                        Authorization: `Bearer ${token}`
                    }
                })
                    .then((response) => {
                        setImages([...images, ...response.data])
                        history.push("/mygallery")
                    })
                    .catch((err) => {
                        console.error(err);
                    });
            })
            .catch(err => {

                console.log("Error", err)

            });
    }

    return (
        <div className="containLogin">
            <div className="containCard">
                <div className="containCardMenu">
                    <Container fixed maxWidth="xs" className="cardLogin">
                        <Typography component="h1" variant="h5" style={{ textAlign: 'center', color: '#FFFFFF', fontWeight: 'bold' }}>
                            Create Gallery
                     </Typography>
                        <form noValidate style={{ height: 'auto' }}>
                            <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                id="Gallery_Name"
                                label="Gallery Name"
                                name="Gallery Name"
                                autoFocus
                                className="inputName"
                                value={galleryName}
                                onChange={(e) => setGalleryName(e.target.value)}
                            />

                            <div style={{ textAlign: 'center', marginTop: 40, }}>
                                <form
                                    method="POST"
                                    action="/upload"
                                    encType="multipart/form-data"
                                // onSubmit={() => handleSubmit(id)}
                                >


                                    <Upload multiple
                                        onChange={({ file, fileList, event }) => {

                                            setImages(fileList.map((f) => f.originFileObj));
                                        }}

                                    >
                                        <Button>
                                            <UploadOutlined /> Upload
                                    </Button>
                                    </Upload>


                                    <div style={{ marginTop: 30 }}>
                                    </div>

                                </form>
                            </div>
                            <div style={{ textAlign: 'center', marginTop: 50, marginBottom: 50 }}>
                                <Button onClick={addGallery} style={{ textAlign: 'center', backgroundColor: "#000000", height: '50px', width: '100%', color: '#FFFFFF' }}>Create</Button>
                            </div>

                        </form>
                    </Container>
                </div>

            </div>
        </div>
    )

}
export default AddGallery;
