import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
// import Modal from '@material-ui/core/Modal';
import { GridListTileBar, Button, Fade, TextField, Menu, MenuItem, InputLabel, Select, FormControl, NativeSelect, IconButton, } from '@material-ui/core';
import Delete from '@material-ui/icons/Delete';
import { createGallery, deleteGallery, listGallery, updateGalleryStatus, updateGalleryName } from '../api';
import Backdrop from '@material-ui/core/Backdrop';
import EditIcon from '@material-ui/icons/Edit';
import Data from '../Data';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SendIcon from '@material-ui/icons/Send';
import { useSelector, useDispatch } from 'react-redux';
import { setToken } from '../action/loginAction'
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { Row, Col, Card } from 'antd';
import 'antd/dist/antd.css';
import Meta from 'antd/lib/card/Meta';
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import Modal from 'antd/lib/modal/Modal';


const GalleryList = () => {
    const [gallerys, setGallerys] = useState([]);
    const [nameGallery, setNameGallery] = useState("");
    const [nameEdit, setNameEdit] = useState("");
    const [id, setID] = useState();

    const classes = useStyles();
    const [openAdd, setOpenAdd] = React.useState(false);
    const [openEdit, setOpenEdit] = useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);

    const [modalValue, setModalValue] = useState(false)


    const token = useSelector(state => state.token);
    console.log("Tokennnnnnnnnnnnnnnnnnnnnnnnnnnnnnn", token);


    const dispatch = useDispatch();


    useEffect(() => {
        listGallery(token)
            .then((response) => {
                console.log("response", response)
                setGallerys(response.data)

            })
            .catch((err) => console.error(err))
    }, [])


    const handleCloseAdd = () => {
        setOpenAdd(false);
    };


    const handleCloseSetting = () => {
        setAnchorEl(null);
    };

    const showModal = (id) => {
        console.log("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD", id);

        setID(id)
        setModalValue(true)
    }

    const handleCancel = () => {
        setModalValue(false)
    }


    const handleSubmit = (e) => {
        e.preventDefault();
        if (nameGallery.trim() === "") {
            return;
        }

        createGallery({ name: nameGallery }, token)
            .then((response) => {
                setGallerys([...gallerys, response.data])
                console.log(response.data)
                handleCloseAdd()
            })
            .catch((err) => console.error(err))
        setNameGallery("");
    }




    const handleDelete = (value) => {
        const filtered = gallerys.filter((item) => item.id !== value.id);
        setGallerys(filtered);
        console.log("delete", value.id)

        deleteGallery(value.id, token)
        handleCloseSetting()
        alert("Delete")
    }



    const handleChangeStatus = ({ id, status_publish }) => {
        console.log("idddddd", id);

        updateGalleryStatus({ id, status_publish: !status_publish }, token)
            .then((response) => {
                console.log(response);

                const update = gallerys.map((item) => {
                    if (item.id === id) {
                        return { ...item, status_publish: !status_publish };
                    }
                    return item;
                });
                setGallerys(update);
            })
            .catch((err) => console.error(err))
    }



    const handleOk = (id) => {
        updateGalleryName({ id, name: nameEdit }, token)

            .then(res => {
                console.log(res)
                setModalValue(false)
            })
            .catch(err => {
                console.log(err)
            })
    }



    return (
        <div className="containerGallery">
            <Modal
                title="Edit Name Gallery"
                visible={modalValue}
                onOk={() => handleOk(id)}
                onCancel={handleCancel}
            >
                <input type="text" value={nameEdit} onChange={(e) => setNameEdit(e.target.value)} />
            </Modal>
            <div className="bodyGallery">

                <div style={{ marginTop: '50px', textAlign: 'inherit' }}>
                    <Button variant="outlined" color="primary" style={{ marginTop: -2 }}><a href="/addgallery" style={{ color: '#FFFFFF' }}>Create Gallery</a></Button>
                </div>
                <div className="postMyPhotoGallery">

                    {
                        gallerys == "" ?
                            <div className="postGalleryNoData">
                                <div>
                                    <h2 style={{ color: "#FFFFFF" }}>No Gallery</h2>
                                </div>
                            </div>
                            :
                            <Row>
                                {
                                    gallerys.map((item, index) => {
                                        return (
                                            <Col span={6}>
                                                <Card
                                                    style={{ margin: 10 }}
                                                    cover={
                                                        <a className="link_menu" href={"/photolistpage/" + item.id} >
                                                            <img src="https://assets.materialup.com/uploads/ae582946-db31-4d15-989b-ca420dea8295/gallery_big.png" alt={item.name} className="myGallery" />
                                                        </a>

                                                    }

                                                >

                                                    <Meta
                                                        title={item.name}
                                                        description={
                                                            item.status_publish == true ?
                                                                <Row>
                                                                    <Col style={{ flex: 1, marginLeft: 70 }}>
                                                                        <Button startIcon={<VisibilityIcon />} size="small" variant="contained" onClick={() => handleChangeStatus(item)}>Publish</Button>
                                                                    </Col>
                                                                    <Col>
                                                                        <Button onClick={() => showModal(item.id)}><EditIcon style={{ color: "#000000" }} /></Button>
                                                                        <Button aria-controls="simple-menu" aria-haspopup="true" variant="contained" className="buttonSetting" onClick={() => handleDelete(item)}>
                                                                            <Delete style={{ color: "rgb(255, 79, 79)" }} />
                                                                        </Button>
                                                                    </Col>
                                                                </Row>
                                                                :
                                                                <div>
                                                                    <Row>
                                                                        <Col style={{ flex: 1, marginLeft: 70 }}>
                                                                            <Button startIcon={<VisibilityOffIcon />} size="small" variant="contained" onClick={() => handleChangeStatus(item)}>Privete</Button>
                                                                        </Col>
                                                                        <Col>
                                                                            <Button onClick={() => showModal(item.id)}><EditIcon style={{ color: "#000000" }} /></Button>
                                                                            <Button aria-controls="simple-menu" aria-haspopup="true" variant="contained" className="buttonSetting" onClick={() => handleDelete(item)}>
                                                                                <Delete style={{ color: "rgb(255, 79, 79)" }} />
                                                                            </Button>
                                                                        </Col>
                                                                    </Row>
                                                                </div>

                                                        }
                                                    />


                                                </Card>
                                            </Col>



                                        )

                                    })
                                }
                            </Row>
                    }

                </div >

            </div >
        </div >
    )
}

export default GalleryList


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 'auto',
        height: 'auto',
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '20%',
        height: '30%'

    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,

    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));