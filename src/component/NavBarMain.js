import React, { useState } from 'react';
import '../App.css';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Grid } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { useSelector, useDispatch } from 'react-redux';
import { setToken, setUser } from '../action/loginAction'


const NavBarMain = () => {

    const user = localStorage.getItem("email")
    const tokenInMain = localStorage.getItem("token")

    console.log("email show", user);



    const dispatch = useDispatch();

    const [auth, setAuth] = React.useState(true);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleChange = (event) => {
        setAuth(event.target.checked);
    };
    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };


    const handleClose = () => {
        setAnchorEl(null);

    };

    const onLogout = async () => {
        await
            localStorage.removeItem("token")
        localStorage.removeItem("email")
        dispatch(setUser(""))

        setAnchorEl(null);
        alert("Logout!!!!!!!!!!!!!")
    }

    const classes = useStyles();

    console.log("token in NavBar", tokenInMain);

    return (
        <div className="containerBar">
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h5" className={classes.title} >

                            <a className="link_menu" href="/">Gallery Photo</a>

                        </Typography>
                        {
                            tokenInMain == "" || tokenInMain == null ?

                                <div>
                                    <Grid container
                                        direction="row"
                                        justify="center"
                                        alignItems="center"
                                    >
                                        <Grid container item xs={3} spacing={3} style={{ marginRight: "50px" }}>
                                            <a className="LoginPage" href="/loginpage">
                                                <Button color="inherit">Login</Button>
                                            </a>
                                        </Grid>

                                        <Grid container item xs={3} spacing={3} >
                                            <p style={{ color: "#000000" }}>|</p>
                                        </Grid>

                                        <Grid container item xs={3} spacing={3} style={{ marginRight: "20px" }} >
                                            <a className="RegisterPage" href="/registerpage">
                                                <Button color="inherit">Register</Button>
                                            </a>
                                        </Grid>

                                    </Grid>
                                </div>
                                :
                                <div>
                                    <IconButton
                                        aria-label="account of current user"
                                        aria-controls="menu-appbar"
                                        aria-haspopup="true"
                                        onClick={handleMenu}
                                        color="inherit"
                                    >
                                        <AccountCircle style={{ color: "#000000" }} />
                                    </IconButton>
                                    <Menu
                                        id="menu-appbar"
                                        anchorEl={anchorEl}
                                        anchorOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        keepMounted
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        open={open}
                                        onClose={handleClose}
                                        style={{ top: "45px" }}
                                    >
                                        <MenuItem>{user}</MenuItem>
                                        <MenuItem><a href="/mygallery" style={{ textDecoration: 'none', color: '#000000' }}>My Gallery</a></MenuItem>
                                        <MenuItem onClick={onLogout}>Logout</MenuItem>
                                    </Menu>
                                </div>
                        }


                    </Toolbar>
                </AppBar>
            </div>

        </div >
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default NavBarMain