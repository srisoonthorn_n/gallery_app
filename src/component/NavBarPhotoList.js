import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AddImage from './AddImage';
import { useDispatch } from 'react-redux';
import { setToken, setUser } from '../action/loginAction';
import { Grid, IconButton, MenuItem, Menu } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { getOne } from '../api';
import { useParams } from 'react-router-dom';



const NavBarPhotoList = () => {

    const user = localStorage.getItem("email")
    const tokenInMain = localStorage.getItem("token")
    const [idGallery, setIdGallery] = useState([])
    const { id } = useParams();

    const [auth, setAuth] = React.useState(true);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);



    const dispatch = useDispatch();

    useEffect(() => {
        getOne(id)
            .then((response) => {
                setIdGallery(response.data)
            })
            .catch((err) => console.log(err));
    }, [])


    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };


    const handleClose = () => {
        setAnchorEl(null);

    };


    const onLogout = async () => {
        await
            localStorage.removeItem("token")
        localStorage.removeItem("email")
        dispatch(setToken(""))
        dispatch(setUser(""))

        setAnchorEl(null);
        alert("Logout!!!!!!!!!!!!!")
    }


    const classes = useStyles();
    return (
        <div className="containerBarPhotoList">
            <div>
                <AppBar position="static">
                    <Toolbar id="PhotoList" className="barPhotoList">
                        <Typography variant="h5" className={classes.title} >
                            <a className="link_menuPhotoList" href="/">
                                Gallery Photo
                            </a>
                        </Typography>
                        <Typography variant="h5" className={classes.title} style={{ color: '#FFFFFF' }}>
                            {idGallery.name}
                        </Typography>


                        {
                            tokenInMain === "" || tokenInMain === null ?

                                <div>
                                    <Grid container
                                        direction="row"
                                        justify="center"
                                        alignItems="center"
                                    >
                                        <Grid container item xs={3} spacing={3} style={{ marginRight: "50px" }}>
                                            <a className="LoginPagePhoto" href="/loginpage">
                                                <Button color="inherit">Login</Button>
                                            </a>
                                        </Grid>

                                        <Grid container item xs={3} spacing={3} >
                                            <p style={{ color: "#FFFFFF" }}>|</p>
                                        </Grid>

                                        <Grid container item xs={3} spacing={3} style={{ marginRight: "20px" }} >
                                            <a className="RegisterPagePhoto" href="/registerpage">
                                                <Button color="inherit">Register</Button>
                                            </a>
                                        </Grid>

                                    </Grid>
                                </div>

                                :
                                <div style={{ flex: 50, textAlign: 'end' }}>

                                    <IconButton
                                        aria-label="account of current user"
                                        aria-controls="menu-appbar"
                                        aria-haspopup="true"
                                        onClick={handleMenu}
                                        color="inherit"
                                    >
                                        <AccountCircle style={{ color: "#FFFFFF" }} />
                                    </IconButton>
                                    <Menu
                                        id="menu-appbar"
                                        anchorEl={anchorEl}
                                        anchorOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        keepMounted
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        open={open}
                                        onClose={handleClose}
                                        style={{ top: "45px" }}
                                    >
                                        <MenuItem>{user}</MenuItem>
                                        <MenuItem onClick={onLogout}>Logout</MenuItem>
                                    </Menu>
                                </div>

                        }

                    </Toolbar>
                </AppBar>
            </div>

        </div >
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        width: '140px',
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
}));

export default NavBarPhotoList