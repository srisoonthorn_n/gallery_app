import React, { useState } from 'react'
import { Container, Typography, TextField, Button, FormControlLabel, Checkbox } from '@material-ui/core'
import '../component/Login.css';
import { login } from '../api';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { setToken, setUser } from '../action/loginAction';
import { useHistory } from 'react-router-dom';
import { host } from '../api/index'

const LoginPage = () => {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const token = useSelector(state => state.token)


    const dispatch = useDispatch();
    const history = useHistory();

    const handleLogin = async () => {

        await axios.post(`${host}/login`, { email, password })
            .then((response) => {
                console.log(response.data);

                if (token != "" || token != null) {

                    dispatch(setToken(response.data.token))
                    dispatch(setUser(response.data.email))

                    localStorage.setItem("token", response.data.token)
                    localStorage.setItem("email", response.data.email)
                    setEmail("")
                    setPassword("")
                    history.push("/")
                }

            })
            .catch((err) => {
                console.log("Error", err);

            });

    }
    return (
        <div className="containLogin">
            <div className="containCard">
                <div className="containCardMenu">
                    <Container fixed maxWidth="xs" className="cardLogin">
                        <Typography component="h1" variant="h5" style={{ textAlign: 'center', color: '#FFFFFF', fontWeight: 'bold' }}>
                            Galley Photo
                         </Typography>
                        <form noValidate style={{ height: 'auto' }}>
                            <p style={{ color: '#FFFFFF' }}>Login with account gallery photo</p>
                            <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoFocus
                                className="inputName"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                            <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                className="inputPass"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                            <div style={{ textAlign: 'center', marginTop: '50px', marginBottom: '10px' }}>
                                <Button onClick={handleLogin} style={{ textAlign: 'center', backgroundColor: "#000000", height: '50px', width: '100%', color: '#FFFFFF' }}>Login</Button>
                            </div>

                            <div style={{ textAlign: 'center' }}>
                                <p style={{ fontSize: '16px', color: '#646464' }}>Don't have an Gallery account ? </p>

                                <p style={{ fontSize: '16px', color: '#646464' }}><a href="/RegisterPage" style={{ color: "#FFFFFF", textDecoration: 'none' }}> Sign Up</a></p>


                            </div>


                        </form>
                    </Container>
                </div>

            </div>
        </div>

    )
}

export default LoginPage