import React, { useState } from 'react'
import { Container, Typography, TextField, Button, FormControlLabel, Checkbox } from '@material-ui/core'
import '../component/Login.css';
import { signup } from '../api';
import { useHistory } from 'react-router-dom';

const RegisterPage = () => {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const history = useHistory();

    const onSumbit = () => {
        alert("Submit")
        signup({ email: email, password: password })
        setEmail("")
        setPassword("")
        history.push("/loginpage")
    }

    const validate = () => {
        var x = document.getElementById("password");
        var y = document.getElementById("confirmPassword");
        if (x.value == y.value) return;
        else alert("password not same");
    }


    return (
        <div className="containLogin">
            <div className="containCard">
                <div className="containCardMenu">
                    <Container fixed maxWidth="xs" className="cardLogin">
                        <Typography component="h1" variant="h5" style={{ textAlign: 'center', color: '#FFFFFF', fontWeight: 'bold' }}>
                            Galley Photo
                         </Typography>
                        <form noValidate style={{ height: 'auto' }}>
                            <p style={{ color: '#FFFFFF' }}> Register</p>
                            <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoFocus
                                className="inputName"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                            <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                className="inputPass"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                            <TextField
                                onBlur={validate}
                                variant="filled"
                                margin="normal"
                                required={true}
                                fullWidth
                                name="ConfirmPassword"
                                label="Confirm Password"
                                type="password"
                                id="confirmPassword"
                                autoComplete="current-password"
                                className="inputPass"
                            />
                            <div style={{ textAlign: 'center', marginTop: '80px', marginBottom: '80px' }}>
                                <Button onClick={onSumbit} style={{ textAlign: 'center', backgroundColor: "#000000", height: '50px', width: '100%', color: '#FFFFFF' }}>Create</Button>
                            </div>

                        </form>
                    </Container>
                </div>

            </div>
        </div>

    )
}

export default RegisterPage