import React from 'react';
import NavBarPhotoList from '../component/NavBarPhotoList';
import '../component/PhotoList.css';
import AllPhoto from '../component/AllPhoto';

const AllPhotoListPage = () => {
    return (
        <div>
            <NavBarPhotoList />
            <AllPhoto />
        </div>
    )
}

export default AllPhotoListPage