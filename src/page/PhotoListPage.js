import React from 'react';
import PhotoList from '../component/PhotoList';
import NavBarPhotoList from '../component/NavBarPhotoList';
import '../component/PhotoList.css';

const PhotoListPage = () => {
    return (
        <div>
            <NavBarPhotoList />
            <PhotoList />
        </div>
    )
}

export default PhotoListPage