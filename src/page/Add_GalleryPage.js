import React from 'react';
import AddImage from '../component/AddImage';
import AddGallery from '../component/AddGallery';

import NavBarMain from '../component/NavBarMain';

const Add_GalleryPage = () => {
    return (
        <div>
            <NavBarMain />
            <AddGallery />
        </div>
    )
}

export default Add_GalleryPage