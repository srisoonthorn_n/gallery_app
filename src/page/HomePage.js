import React from 'react';
import NavBarMain from '../component/NavBarMain';
import GalleryList from '../component/GalleryList';
import '../App.css';
import AllGallery from '../component/AllGallery';

const HomePage = () => {
    return (
        <div>
            <NavBarMain />
            <AllGallery />
        </div>
    )
}

export default HomePage

//ทำให้อัลบั้มไหลจากตรงกลางไป