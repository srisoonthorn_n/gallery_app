import React from 'react';
import NavBarMain from '../component/NavBarMain';
import GalleryList from '../component/GalleryList';
import '../App.css';

const MyGallery = () => {
    return (
        <div>
            <NavBarMain />
            <GalleryList />
        </div>
    )
}

export default MyGallery
