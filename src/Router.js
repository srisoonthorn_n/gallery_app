import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Add_ImagePage from './page/Add_ImagePage'
import HomePage from './page/HomePage'
import PhotoListPage from './page/PhotoListPage';
import LoginPage from './page/LoginPage';
import RegisterPage from './page/RegisterPage';
import MyGallery from './page/MyGallery';
import AllPhotoListPage from './page/AllPhotoListPage';

import Add_GalleryPage from './page/Add_GalleryPage';

const Router = () => {
    return (
        <BrowserRouter>
            <Route path="/" component={HomePage} exact />
            <Route path="/mygallery" component={MyGallery} />
            <Route path="/photolistpage/:id" component={PhotoListPage} />
            <Route path="/addimage/:id" component={Add_ImagePage} />
            <Route path="/allphoto/:id" component={AllPhotoListPage} />
            <Route path="/loginpage" component={LoginPage} />
            <Route path="/addgallery" component={Add_GalleryPage} />
            <Route path="/registerpage" component={RegisterPage} />
        </BrowserRouter>
    )
}

export default Router