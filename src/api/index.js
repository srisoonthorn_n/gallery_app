import axios from "axios";

export const host = "http://localhost:8080";

//------------------------- Gallery----------------------------

export function listAllGallery() {
    return (
        axios.get(`${host}/galleries`)
    )
}

export function listGallery(token) {
    return axios.get(`${host}/auth/galleries`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function createGallery(name, token) {
    return axios.post(`${host}/auth/galleries`, name, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });

}

export function deleteGallery(id, token) {
    return axios.delete(`${host}/auth/galleries/${id}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });

}

export function updateGalleryName({ id, name }, token) {
    return axios.patch(`${host}/auth/galleries/${id}/names`, { name }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function updateGalleryStatus({ id, status_publish }, token) {
    return axios.patch(`${host}/auth/galleries/${id}/publishes`, { status_publish }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

//------------------------- Login----------------------------

export function signup(email, password) {
    return (
        axios.post(`${host}/signup`, email, password)
    )

}


export function login(email, password) {
    return (

        axios.post(`${host}/login`, email, password)
    )

}

//-------------------------Images----------------------------

export function addImage(id, formData, token) {
    return axios.post(`${host}/auth/galleries/${id}/images`, formData, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}


export function listImages(id, token) {
    return axios.get(`${host}/auth/galleries/${id}/images`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function listImagesPublish(id) {
    return axios.get(`${host}/galleries/${id}/images`)

}


export function getOne(id) {
    return axios.get(`${host}/galleries/${id}`)
}

export function deleteImage(id, token) {
    return axios.delete(`${host}/auth/images/${id}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}


