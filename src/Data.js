const Data = [
    {
        img: 'https://images.unsplash.com/photo-1494537176433-7a3c4ef2046f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80',
        name: 'Breakfast',
        author: 'jill111',
        cols: 2,
        featured: true,
    },
    {
        img: 'https://images.unsplash.com/photo-1498569026542-39c64353e401?ixlib=rb-1.2.1&auto=format&fit=crop&w=1267&q=80',
        name: 'Tasty burger',
        author: 'director90',
    },
    {
        img: 'https://images.unsplash.com/photo-1493219686142-5a8641badc78?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
        name: 'Camera',
        author: 'Danson67',
    },
    {
        img: 'https://images.unsplash.com/photo-1470790376778-a9fbc86d70e2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=649&q=80',
        name: 'Morning',
        author: 'fancycrave1',
        featured: true,
    },
    {
        img: 'https://images.unsplash.com/photo-1471666875520-c75081f42081?ixlib=rb-1.2.1&auto=format&fit=crop&w=1438&q=80',
        name: 'Hats',
        author: 'Hans',
    },
    {
        img: 'https://images.unsplash.com/photo-1528297506728-9533d2ac3fa4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
        name: 'Honey',
        author: 'fancycravel',
    },
    {
        img: 'https://images.unsplash.com/photo-1494537449588-7f07cede2556?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80',
        name: 'Vegetables',
        author: 'jill111',
        cols: 2,
    },
    {
        img: 'https://images.unsplash.com/photo-1495471547134-98698271c52a?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
        name: 'Water plant',
        author: 'BkrmadtyaKarki',
    },
    {
        img: 'https://images.unsplash.com/reserve/LJIZlzHgQ7WPSh5KVTCB_Typewriter.jpg?ixlib=rb-1.2.1&auto=format&fit=crop&w=641&q=80g',
        name: 'Mushrooms',
        author: 'PublicDomainPictures',
    },
    {
        img: 'https://images.unsplash.com/photo-1472289065668-ce650ac443d2?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
        name: 'Olive oil',
        author: 'congerdesign',
    },
    {
        img: 'https://images.unsplash.com/photo-1505744386214-51dba16a26fc?ixlib=rb-1.2.1&auto=format&fit=crop&w=1467&q=80',
        name: 'Sea star',
        cols: 2,
        author: '821292',
    },
    {
        img: 'https://images.unsplash.com/photo-1493723843671-1d655e66ac1c?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
        name: 'Bike',
        author: 'danfador',
    },
    {
        img: 'https://images.unsplash.com/photo-1493723843671-1d655e66ac1c?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
        name: 'Bike',
        author: 'danfador',
    },
    {
        img: 'https://images.unsplash.com/photo-1493723843671-1d655e66ac1c?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
        name: 'Bike',
        author: 'danfador',
    },
    {
        img: 'https://images.unsplash.com/photo-1493723843671-1d655e66ac1c?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
        name: 'Bike',
        author: 'danfador',
    },
    {
        img: 'https://images.unsplash.com/photo-1493723843671-1d655e66ac1c?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
        name: 'Bike',
        author: 'danfador',
    },
];

export default Data;
